
var app = protos.app;
var util = require('util');
var sanitizer = protos.require('sanitizer');

function MainHelper(app) {

}

protos.extend(MainHelper.prototype, {
  
  columns_class: function(_, c) {
    var out = c.split('/').reduce(function(state, current, i) {
      if (current !== '-') {
        switch (i) {
          case 0: var prefix = 'col-xs-'; break;
          case 1: prefix = 'col-sm-'; break;
          case 2: prefix = 'col-md-'; break;
          case 3: prefix = 'col-lg-'; break;
        }
        var cols = 12/parseInt(current, 10);
        state.push(prefix + cols.toString());
      }
      return state;
    }, []);
    return out.join(' ');
  },
  
  columns_clear: function(_, i, c) {
    var mods = c.split('/').reduce(function(state, current, i) {
      if (current !== '-') {
        switch (i) {
          case 0: var context = 'xs'; break;
          case 1: context = 'sm'; break;
          case 2: context = 'md'; break;
          case 3: context = 'lg'; break;
        }
        var mod = parseInt(current, 10);
        if (mod > 1) state[context] = mod;
      }
      return state;
    }, {});
    var out = [];
    for (var context in mods) {
      if ((i+1) % mods[context] === 0) {
        out.push(util.format('visible-%s', context));
      }
    }
    return out.length ? util.format('<div class="clear %s"></div>\n', out.join(' ')) : '';
  },
  
  randp: function() {
    var num = Math.floor(2*Math.random());
    if (num === 1) return '\n<p>Accusantium quam, ultricies eget tempor id, aliquam eget nibh et. Maecen aliquam, risus at semper ullamcorper magna.</p>\n';
  },
  
  if_eq: function(content, a, b) {
    if (a && b && a.toString() === b.toString()) {
      return content;
    } else {
      return '';
    }
  },
  
  json: function(_, opts) {
    return sanitizer.escape(JSON.stringify(opts));
  },
  
  print_styles: function(_, key) {
    var styles = app.data(key);
    if (styles) {
      var arr = styles.reduce(function(state, path) {
        path = path.trim();
        if (path.indexOf('//') !== 0) {
          state.push(util.format('<link rel="stylesheet" type="text/css" href="%s" />', path));
        }
        return state;
      }, []);
      return arr.join('\n');
    } else {
      return '';
    }
  },
  
  print_scripts: function(_, key) {
    var scripts = app.data(key);
    if (scripts) {
      var arr = scripts.reduce(function(state, path) {
        path = path.trim();
        if (path.indexOf('//') !== 0) {
          state.push(util.format('<script type="text/javascript" src="%s"></script>', path));
        }
        return state;
      }, []);
      return arr.join('\n');
    } else {
      return '';
    }
  }
  
  
});

module.exports = MainHelper;