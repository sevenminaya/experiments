
/* core.ui.js */

(function($, Core) {
  
var body = Core.body, html = Core.html, win = Core.window;
var scrollTarget = (Core.browser.opera || Core.browser.msie) ? body : body.add(html);

var styles = [];

Core.ui = {

  queueStyle: function(str) {
    styles.push(str);
  },

  renderStyles: function() {
    if (styles.length) {
      $('head').append('<style type="text/css">'+ styles.join('\n') +'</style>');
    }
  },
  
  randId: function(elem) {
    var id = 'sv-rand-' + Core.util.randint();
    elem.attr('id', id);
    return id;
  },
  
  scrollTop: function(value) {
    value = parseInt(value, 10) || 0;
    scrollTarget.stop().animate({scrollTop: value}, 500, 'easeOutQuad');
  },

  verticalCenter: function(elements, outer, affectedBy) {
    elements.each(function(i, node) {
      var item = this.__self || (this.__self = $(node));
      var recenter, timeout = null;
      Core.nextTick(function() {
        Core.at('resize', recenter = function() {
          item.css({marginTop: 0});
          var offset = -1*Math.floor((item.outerHeight() - outer.outerHeight())/2);
          item.css({marginTop: offset, opacity: 1});
        });
        Core.at('refresh_min_height_adjust', recenter); // Adjust when min height changes
        if (affectedBy) affectedBy.bind(Core.transitionEndEvent, recenter); // Refresh when transition ends
      });
    });
  },
  
  resizeTrigger: function(opts, callback) {
    var timeout = null, busy = false;
    var count = opts.iterations;
    Core.at('resize', function(w, h) {
      if (busy) return;
      clearTimeout(timeout);
      callback(w, h);
      timeout = setTimeout(function() { // Timeout to re-trigger resize event
        busy = true; // Lock event, prevent further triggers
        var intval = setInterval(function() { // Try 5 resize iterations every 150ms
          callback(win.width(), win.height()); // Emit with updated dimensions
          if (--count === 0) {
            busy = false; // Unlock event
            clearInterval(intval); // Stop iterations
          }
        }); 
      }, opts.timeout || 150);
    });
  },

  resizeInterpolate: function(o, callback) {
    var func = Core.util.interpolateFunc(o.wmin, o.wmax, o.vmin || 0, o.vmax || 100);
    Core.at('resize', function(w, h) {
      callback(func(w), w);
    });
  },

  resizeOnImagesLoaded: function(target, callback) {
    return new imagesLoaded(target).on('always', function() {
      if (callback instanceof Function) callback();
      Core.window.trigger('resize');
    });
  },

  hoverDelay: function(item, enterCb, leaveCb, inDelay, outDelay) {
    var timeout = null;
    item.hover(function(e) {
      clearTimeout(timeout);
      switch (e.type) {
      case 'mouseenter':
        timeout = setTimeout(enterCb, inDelay, this);
        break;
      case 'mouseleave':
        timeout = setTimeout(leaveCb, outDelay || 1, this); // Hide right away to allow next animation to render smoothly
        break;
      }
    });
  }

}
  
})(jQuery, window.Core);