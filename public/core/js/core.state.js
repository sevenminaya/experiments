
/* core.state.js */

(function($, Core) {

var state = locache.get('state') || {};

Core.state = {
  
  get: function(key) {
    return state[key];
  },
  
  set: function(key, val) {
    state[key] = val;
    locache.set('state', state);
    return val;
  },
  
  setDefault: function(key, val) {
    var out = this.get(key);
    if (out === null) {
      this.set(key, val);
    } else {
      return out;
    }
  }

}  
  
})(jQuery, window.Core);