
/* core.util.js */

(function($, Core) {
  
var util, regex = {
  integer: /^[0-9]+$/,
  float: /^\d+\.\d+$/,
  boolean: /^(true|false)$/
}

var idCounter = 0, randBase = Math.floor(Math.random()*1e6);

Core.util = util = {
  
  format: _.string.sprintf,
  
  keysForPrefix: function(prefix, data) { // requires jquery object
    var out = {}, re = new RegExp('^' + prefix);
    for (var key in data) {
      if (re.test(key) && key !== prefix && data[key]) { // Ensure only valid data is returned
        var k = _.string.camelize(_.string.dasherize(key).split('-').slice(1).join('-')).trim();
        out[k] = data[key]; 
      }
    }
    return out;
  },
  
  interpolateFunc: svInterpolateFunc,
  
  interpolatePercent: function(vmin, vmax) {
    return this.interpolateFunc(0, 100, vmin, vmax);
  },
  
  randint: function() {
    return ++idCounter + randBase;
  }
  
}
  
})(jQuery, window.Core);