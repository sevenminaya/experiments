
/* core.template.js */

(function($, Core) {
  
var compare = function(op, a, b) {
  switch (op) {
    case 'eq': return a === b;
    case 'gt': return a > b;
    case 'gte': return a >= b;
    case 'lt': return a < b;
    case 'lte': return a <= b;
    default: return false;
  }
}

var boolFunc = function(cond, negate) {
  return function(a, b, options) {
    var out, result = compare(cond, a, b);
    if (negate ? !result : result) {
      out = options.fn(this);
    } else {
      out = options.inverse(this);
    }
    return out;
  }
}

_.extend(Handlebars.helpers, {
  
  if_eq: boolFunc('eq'),
  
  unless_eq: boolFunc('eq', true),
  
  if_gt: boolFunc('gt'),
  
  unless_gt: boolFunc('gt', true),
  
  if_gte: boolFunc('gte'),
  
  unless_gte: boolFunc('gte', true),
  
  if_lt: boolFunc('lt'),
  
  unless_lt: boolFunc('lt', true),
  
  if_lte: boolFunc('lte'),
  
  unless_lte: boolFunc('lte', true)

});
  
var TEMPLATES = {

  sv_hover_ui: '\
<aside class="sv-hover-ui">\n\
  <div class="top-half">\n\
    <span class="sv-hover-btns">\n\
      {{#if title_url}}<a class="sv-hover-btn anchor" href="{{{title_url}}}"></a>{{/if}}\n\
      {{#if lightbox_url}}<a class="sv-hover-btn zoom" rel="lightbox"><img src="{{{lightbox_url}}}" /></a>{{/if}}\n\
    </span><!-- .hover-btns -->\n\
  </div><!-- .top-half -->\n\
  <div class="bottom-half">\n\
    <div class="sv-meta text-center">\n\
      {{#if title}}<span class="title"><a{{#if title_url}} href="{{{title_url}}}"{{/if}}>{{{title}}}</a></span>\n\
      {{#if desc}}<span class="desc"><a{{#if desc_url}} href="{{{desc_url}}}"{{/if}}>{{{desc}}}</a></span>{{/if}}{{/if}}\n\
    </div><!-- .meta -->\n\
  </div><!-- .bottom-half -->\n\
</aside>',

  padding_reduction: '\
@media (max-width: 767px) {\n\
  #{{{id}}} {\n\
    padding: {{xs.top}} 0{{#unless_eq xs.top xs.bottom}} {{xs.bottom}}{{/unless_eq}} !important;\n\
  }\n\
}\n\
@media (min-width: 768px) and (max-width: 991px) {\n\
  #{{{id}}} {\n\
    padding: {{sm.top}} 0{{#unless_eq sm.top sm.bottom}} {{sm.bottom}}{{/unless_eq}} !important;\n\
  }\n\
}\n\
@media (min-width: 992px) and (max-width: 1199px) {\n\
  #{{{id}}} {\n\
    padding: {{md.top}} 0{{#unless_eq md.top md.bottom}} {{md.bottom}}{{/unless_eq}} !important;\n\
  }\n\
}'

}

Core.template = {
  
  render: function(tpl, context, wrapperFun) {
    var template = TEMPLATES[tpl];
    if (typeof template == 'string') {
      template = TEMPLATES[tpl] = Handlebars.compile(TEMPLATES[tpl]);
    }
    var buf = template(context || {});
    return (wrapperFun instanceof Function) ? wrapperFun(buf) : buf;
  }

}
  
})(jQuery, window.Core);