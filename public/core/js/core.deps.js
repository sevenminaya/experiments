
/* core.deps.js */

(function($, Core) {

var loaded = {}, loading = {};
var events = new EventEmitter();

var loader = function(scripts, callback) {
  head.js.apply(null, scripts.concat(callback));
}

Core.deps = {
  
  load: function(dep, callback) {
    if (dep instanceof Array) { var cb;
      this.load(dep.shift(), cb = function() {
        if (dep.length === 0) {
          callback();
        } else {
          Core.deps.load(dep, cb);
        }
      });
    } else if (dep in loading) {
      events.once(dep, callback);
    } else if (dep in loaded) {
      callback();
    } else if (dep in this) {
      loading[dep] = true;
      events.once(dep, callback);
      loader(this[dep], function() {
        delete loading[dep];
        loaded[dep] = true;
        events.emit(dep);
      });
      
    } else {
      throw new Error("Unable to load: " + dep);
    }
  },

  "jquery-autosize": [
    "/core/js/jquery/jquery.autosize.min.js"
  ],

  "google-code-prettify": [
    "/core/js/vendor/google-code-prettify/run_prettify.js"
  ],
  
  "ie-legacy": [
    "/core/js/legacy/es5-shim.min.js",
    "/core/js/legacy/html5shiv.min.js",
    "/core/js/legacy/ie.js"
  ]

}

})(jQuery, window.Core);