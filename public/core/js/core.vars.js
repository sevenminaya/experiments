
/* core.vars */

(function($, Core) {
  
Core.once(':ready', function(body) {
  
  Core.vars = {
    
    bodyFontSize: parseInt(body.css('fontSize'), 10),
    
    header: body.find('header[role=banner]'),
    
    searchBox: body.find('> #search-box form')
    
  }
  
});
  
})(jQuery, window.Core);