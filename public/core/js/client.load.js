
/* client.load.js */

(function($, Core) {

Core.once(':ready', function() {
  initIE();
  initAutoSize();
});

Core.once('ready', function() {
  initPrettyPrint();
});

function initPrettyPrint() {
  var pre = $('code.prettyprint');
  if (pre.length) {
    Core.deps.load('google-code-prettify');
  }
}

function initIE() {
  if (Core.browser.msie) {
    var version = parseInt(Core.browser.version, 10); // Truncates decimal points
    if (version <= 8) {
      return; // Not supported
    } if (version === 9 && Core.html.hasClass('ie9')) { // Real IE9, not simulated
      Core.emit('ie-legacy-loaded', version); // Already loaded via conditional comment
    } else {
      Core.deps.load('ie-legacy', function() {
        Core.emit('ie-legacy-loaded', version); // Needs to be loaded (conditional comments don't work on ie10+)
      });
    }
  }
}

function initAutoSize() {
  var items = $('.ui-autosize');
  if (items.length) {
    Core.deps.load('jquery-autosize', function() {
      items.autosize();
    });
  }
}

})(jQuery, window.Core);