
/* sv-head.js */

(function(window) {

var win = $(window);
var resizeCallbacks = [];

$(document).ready(function() {
  resizeCallbacks.forEach(function(callback) {
    Core.at('resize', callback);
    Core.at('refresh_min_height_adjust', callback);
  });
});

window.svSetImageMinHeight = function(node, height, wmin, wmax) {
  
  wmin = wmin || 480;
  wmax = wmax || 1200;
  
  var img = $(node);
  var ow = node.width, oh = node.height;
  
  if (ow && oh && height < oh) {
    
    var H = height, W = Math.floor((ow*H)/oh);
    var getImageHeight = svInterpolateFunc(wmin, wmax, height, node.height);
    
    var adjust = function(Wp) {
      var h = getImageHeight(Wp);
      var w = Math.floor((ow*h)/oh);
      img.attr({
        width: w,
        height: h
      });
    }
    
    if (typeof window.Core == 'object') {
      Core.at('resize', adjust);
    } else {
      resizeCallbacks.push(adjust);
    }

    adjust(win.outerWidth());

  }

}

window.svSetFwImageMinHeight = function(node, height) {
  
  var img = $(node);
  var ow = node.width, oh = node.height;

  if (ow && oh) {
    
    var H = height, W = Math.floor((ow*H)/oh);
    
    var adjust = function(Pw) {
      if (Pw < W) {
        var M = -1*Math.floor((W-Pw)/2);
        img.css({
          width: W,
          height: H,
          marginLeft: M
        });
        if (typeof window.Core == 'object') {
          if (Core.browser.mozilla) Core.emit('refresh_min_height_adjust');
        }
      } else {
        img.css({
          width: '100%',
          height: 'auto',
          marginLeft: 0
        });
      }
    }
  
    if (typeof window.Core == 'object') {
      Core.at('resize', adjust);
    } else {
      resizeCallbacks.push(adjust);
    }

    adjust(win.outerWidth());
    
  }
  
}

window.svInterpolateFunc = function(wmin, wmax, vmin, vmax) {
  var h = wmax - wmin;
  var c = vmax - vmin;
  return function(w) {
    var d = w - wmin;
    if (w <= wmin) {
      return vmin;
    } else if (w >= wmax) {
      return vmax;
    } else {
      return vmin + Math.floor((d/h)*c);
    }
  }
}

})(window, jQuery);