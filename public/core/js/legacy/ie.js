
/* core/js/legacy/ie.js */

(function($, Core) {

Core.once('ie-legacy-loaded', function(version) {
  Core.html.addClass('ie ie' + version);
  if (version === 9) {
    initPlaceholders();
  } 
});

function initPlaceholders() {
  $('form input[placeholder]').each(function(i, elem) {
    var self = $(elem);
    var placeholder = $.trim(self.attr('placeholder') || '');
    if (placeholder) {
      self.val(placeholder);
      self.focus(function(e) {
        if ($.trim(self.val()) === placeholder) self.val('');
      }).blur(function(e) {
        if ($.trim(self.val()) == '') self.val(placeholder);
      });
    }
  });
}
  
})(jQuery, window.Core);