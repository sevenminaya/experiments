
/* core.browser.js */

(function($, Core) {

// Uses code from jquery-migrate under the MIT license
// https://github.com/jquery/jquery-migrate

var uaMatch = function( ua ) {
  
	ua = ua.toLowerCase();
  
	var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
		/(webkit)[ \/]([\w.]+)/.exec( ua ) ||
		/(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
		/(msie) ([\w.]+)/.exec( ua ) ||
		ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
    [];
    
	return detectModernIE(ua) || {
		browser: match[ 1 ] || "",
		version: match[ 2 ] || "0"
	}

}

var browser = Core.browser = {};
var matched = uaMatch(navigator.userAgent);

if (matched.browser) {
	browser[matched.browser] = true;
	browser.version = matched.version;
}

// Chrome is Webkit, but Webkit is also Safari.
if (browser.chrome) {
	browser.webkit = true;
} else if (browser.webkit) {
	browser.safari = true;
}

// Functions

/**
  Detects modern IE
  Reference: http://rochcass.wordpress.com/2013/11/27/detecting-ie10-and-ie11browsers/
  Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E; rv:11.0) like Gecko
  Mozilla/5.0 (IE 11.0; Windows NT 6.3; Trident/7.0; .NET4.0E; .NET4.0C; rv:11.0) like Gecko
  Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.3; Trident/6.0)
 */

function detectModernIE(ua) {
  if (document.body.style.msScrollLimit !== undefined){ // IE10+ Browser
    var matches, version;
    if (document.body.style.msTextCombineHorizontal === undefined && /MSIE/i.test(ua) === true) {
      // IE10
      matches = ua.match(/;\s+MSIE\s+(\d+\.\d+)\s*;/i);
      version = matches[1];
    } else {
      // IE Edge
      matches = ua.match(/;\s+rv:(\d+\.\d+)[ ]*\)\s+like\s+Gecko$/i);
      if (matches) {
        version = matches[1]; // Detect future versions
      } else {
        return null; // Let it crash
      }
    }
    return {
      browser: "msie",
      version: version
    }
  } else {
    return null;
  }
}

})(jQuery, window.Core);