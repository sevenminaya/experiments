
/* core.events.js */

(function($, Core) {

var win = Core.window;
var emittedEvents = {};

var events = new EventEmitter();

_.extend(Core, {
  
  nextTick: function(callback) {
    setTimeout(function() {
      callback.call(null);
    }, 0);
  },
  
  on: function() {
    return events.addListener.apply(events, arguments);
  },
    
  at: function(evt, callback, context) {
    var retval = events.on(evt, callback);
    if (evt in emittedEvents) {
      callback.apply(context || this, emittedEvents[evt]);
    }
    return retval;
  },
  
  once: function() {
    return events.addOnceListener.apply(events, arguments);
  },

  emit: function(evt) {
    var retval = events.emit.apply(events, arguments);
    emittedEvents[evt] = [].slice.call(arguments, 1);
    return retval;
  },
  
  initialize: function() {
    $(document).ready(function() {
      var body = Core.body, html = Core.html;
      Core.emit(':ready', body, html);
      Core.emit('ready', body, html);
      Core.emit('ready:', body, html);
      win.trigger('resize');
      win.trigger('scroll');
    });
  }
  
});

win.resize(function() { // Emit resize event on window resize
  Core.emit('resize', win.width(), win.height());
});

window.addEventListener('orientationchange', function() { // Emit resize event on orientation change (mobile)
  Core.emit('orientation', window.orientation);
  win.trigger('resize');
});

win.scroll(function(e) { // Emit scroll event on window scroll
  Core.emit('scroll', win.scrollLeft(), win.scrollTop());
});

})(jQuery, window.Core);