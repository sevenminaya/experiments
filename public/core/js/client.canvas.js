
/* client.canvas.js */

(function($) {
  
var requestAnimFrame = window.requestAnimationFrame;
var cancelAnimFrame = window.cancelAnimationFrame;
  
Core.once('ready', function(body) {
  initProgressCircles(body);
});

function initProgressCircles(body) {
  
  body.find('.sv-progress-circle').each(function(i, node) {
    
    var item = $(node);
    var padding = 5;
    var fac = window.devicePixelRatio || 1;
    
    var o = _.extend({
      percent: 0,
      thickness: 10,
      capLineWidth: 3,
      duration: 2550,
      endCircles: true,
      useGradient: true,
      updateCounter: true,
      animateTitle: true,
      fgColor: '#dddddd', // Grayscale, #21c59d from psd
      bgColor: '#f9f9f9',
      lineCap: 'round',
      easing: 'easeOutQuad',
      gradientColors: ["#f56921", "#e9d02e", "#6ba947", "#38a4cf"]
    }, item.data());
    
    o.thickness *= fac;
    o.capLineWidth *= fac;

    if (o.lineCap !== 'round') {
      o.endCircles = false; // Don't use end circles if linecap is not round
    }

    if (o.percent >= 100) {
      o.percent = 99.99; // Ensure full circle is rendered
    } else if (o.percent <= 0) {
      o.percent = 0.05; // Prevent full circle from being rendered
    }

    var inner = item.find('> .inner');
    var canvasElem = inner.find('canvas');
    var canvas = canvasElem.get(0);

    if (o.updateCounter) {
      
      // Get text to animate
      var span = inner.find('span.title');
      var innerText = inner.find('.inner-text').css({opacity: 0});
      var html = span.html();
      var matches = html.match(/\d+/);
      if (matches) {
        var counter, num = parseInt(matches[0], 10);
        span.html(html.replace(num, '<i class="sv-counter">'+ num +'</i>'));
        counter = span.find('i.sv-counter');
      }

    }
    
    if (o.animateTitle) {
      var title = item.find('.sv-title').css({top: '1em', opacity: 0});
      if (title.length) {
        var deltaDuration = 0.17*o.duration;
        setTimeout(function() {
          title.transition({opacity: 1, top: 0}, o.duration - 1.5*deltaDuration, 'easeOutQuad');
        }, deltaDuration);
      }
    }
    
    canvasElem.css({
      width: canvas.width,
      height: canvas.height
    });
    
    canvas.width *= fac;
    canvas.height *= fac;
    
    var c = canvas.getContext('2d');
    var size = canvas.width;
    var center = size/2;
    var r = (size - o.thickness)/2 - padding; // Don't touch the edge of canvas
    var degs = percentToDegs(o.percent);
    
    c.save();
    
    var updateCounter = o.updateCounter ? function(x, v) {
      // NOTE: Ceiling is needed to get the full number
      // NOTE: Capping the value to 1 to avoid wrong value at end of counter
      innerText.css({opacity: 2.5*x});
      counter.html(Math.ceil(num*(v > 1 ? 1 : v)));
    } : $.noop;
    
    var render = function(degs, x) {
      
      if (degs > 360) {
        degs = 359.99; // Avoid going above 360 degrees
      } else if (degs <= 0) {
        degs = 0.1; // Avoid going below zero degrees
      }
      
      c.clearRect(0, 0, size, size);
      
      var rads = degToRad(degs - 90); // Add the quadrant
    
      var circle = function(x, y, color) {
        c.beginPath();
        c.arc(x, y, (o.thickness/2 + 2), 0, 2*Math.PI);
        c.lineWidth = o.capLineWidth;
        c.strokeStyle = 'white';
        c.shadowBlur = 2;
        c.shadowColor = 'rgba(0,0,0,0.3)';
        c.shadowOffsetY = 1;
        if (color) {
          c.fillStyle = color;
          c.fill();
        }
        c.stroke();
        c.closePath();
      }
    
      // Get moving circle coordinates
      // console.log(degs);
      var grad, cx, cy;
      if (degs > 0 && degs <= 90) {
        cx = center + r*Math.cos(degToRad(90 - degs));
        cy = center - r*Math.sin(degToRad(90 - degs));
      } else if (degs > 90 && degs <= 180) {
        cx = center + r*Math.cos(degToRad(degs - 90));
        cy = center + r*Math.sin(degToRad(degs - 90));
      } else {
        cx = center - r*Math.cos(degToRad(degs - 270));
        cy = center - r*Math.sin(degToRad(degs - 270));
      }
  
      // Circle below
      c.beginPath();
      c.arc(center, center, r, 0, 2*Math.PI); // 360 = PI/2
      c.lineWidth = o.thickness;
      c.strokeStyle = o.bgColor;
      c.stroke();
      c.closePath();  
      
      c.save(); // Save canvas state, prevents shadows from being re-drawn

      if (o.useGradient) {
        // Linear gradient
        grad = c.createLinearGradient(50*fac, 50*fac, 150*fac, 150*fac);
        gradColorStops(grad, o.gradientColors);
      }

      // Circle above
      c.beginPath();
      c.arc(center, center, r, degToRad(270), rads);
      c.lineWidth = o.thickness;
      c.lineCap = o.lineCap;
      c.strokeStyle = o.useGradient ? grad : o.fgColor;
      c.stroke();
      c.closePath();
      
      if (o.endCircles) {
        
        // Sample color at pixel (before first mini circle is drawn)
        var p = c.getImageData(cx, cy, 1, 1).data;
        var rgb = 'rgb('+ [p[0], p[1], p[2]].join(',') +')';
  
        // First mini circle (doesn't move)
        circle(center, padding + (o.thickness/2));
  
        // Second mini circle
      
        c.globalAlpha = 5.5*x; // Accellerate opacity by factor
      
        circle(cx, cy, rgb);

        c.globalAlpha = 1;
        
      }
  
      c.restore(); // Restores canvas state to avoid shadows from being re-applied
      
    }
    
    render(0.1);
    
    tween({
      start: 0.1,
      end: degs
    }, function(degs, x, v) {
      render(degs, x);
      if (o.updateCounter) {
        updateCounter(x, v);
      }
    }, o.duration, o.easing);

  });

}

// t: current time, b: begInnIng value, c: change In value, d: duration

function tween(o, callback, duration, easing) {
  var easefn = $.easing[easing] || $.easing.def;
  var x,t, startTime = new Date().getTime();
  var endTime = startTime + duration;
  var v, b = o.start, c = o.end - o.start;
  var d = duration;
  var animFrameID = null;
  var animCallback = function() {
    t = Date.now() - startTime;
    x = t/duration;
    v = easefn(x, t, b, c, d);
    callback(v, x, v/o.end);
    if (t >= d) {
      cancelAnimFrame(animFrameID);
    } else {
      requestAnimFrame(animCallback);
    }
  }
  requestAnimFrame(animCallback);
}

function gradColorStops(g, colors) {
  var i = 0, inc = 1/(colors.length - 1);
  colors = [].concat(colors);
  g.addColorStop(i, colors.shift());
  while (colors.length) {
    i += inc;
    g.addColorStop(i, colors.shift());
  }
}

function degToRad(degs) {
  return (Math.PI/180)*degs;
}

function percentToDegs(val) {
  return (360*val)/100;
}

function percentToRads(val) {
  return degToRad(percentToDegs(val));
}
  
})(jQuery, window.Core);