
/* core.compat.js */

(function($, Core) {

// Local vars
var ua = navigator.userAgent.toString();
  
// Set retina class for devices with greater pixel ratios
var aspectRatio = window.devicePixelRatio;
if (aspectRatio && aspectRatio > 1) {
  Core.html.addClass('retina');
}

// Detect ios et al
switch (navigator.platform) {
  case 'iPhone':
  case 'iPad':
  case 'iPod':
    Core.ios = true;
    Core.html.addClass('ios');
    Core.html.addClass(navigator.platform.toLowerCase());
    break;
}

// Detect android
if (ua.indexOf('Android') !== -1) {
  Core.android = true;
  Core.html.addClass('android');
}

// Detect safari on desktop
if (Core.browser.safari) {
  Core.html.addClass('safari');
  if (!Core.ios) {
    Core.html.addClass('safari-desktop');
  }
}

// Detect mozilla
if (Core.browser.mozilla) {
  Core.html.addClass('mozilla');
}

// Set mobile, supports more devices in the future
if (Core.ios || Core.android) Core.mobile = true;

// Fallback to $.fn.animate if transitions don't work
if (!$.support.transition) $.fn.transition = $.fn.animate;

// The normalized browser's native transition end event
// http://stackoverflow.com/questions/2794148
Core.transitionEndEvent = (function() {
  
  // http://stackoverflow.com/a/9090128/235571
  var el = document.createElement('fakeelement');
  var transitions = {
    'transition':'transitionend',
    'OTransition':'oTransitionEnd',
    'MozTransition':'transitionend',
    'WebkitTransition':'webkitTransitionEnd'
  }
  for(var t in transitions){
    if( el.style[t] !== undefined ){
      return transitions[t];
    }
  }
})();


// requestAnimationFrame polyfill by Erik Möller. Fixes from Paul Irish and Tino Zijdel

// http://paulirish.com/2011/requestanimationframe-for-smart-animating/
// http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
// https://gist.github.com/paulirish/1579671

// MIT license

(function (window, rAF, cAF) {
  var lastTime = 0, vendors = ['ms', 'moz', 'webkit', 'o'];
  for (var x = 0; x < vendors.length && !window[rAF]; ++x) {
    window[rAF] = window[vendors[x] + 'RequestAnimationFrame'];
    window[cAF] = window[vendors[x] + 'CancelAnimationFrame'] || window[vendors[x] + 'CancelRequestAnimationFrame'];
  }
  if (!window[rAF]) {
    window[rAF] = function (callback) {
      var currTime = new Date().getTime();
      var timeToCall = Math.max(0, 16 - (currTime - lastTime));
      var id = setTimeout(function () {
        callback(currTime + timeToCall);
      }, timeToCall);
      lastTime = currTime + timeToCall;
      return id;
    }
  }
  if (!window[cAF]) {
    window[cAF] = function (id) {
      window.clearTimeout(id);
    }
  }
}(window, 'requestAnimationFrame', 'cancelAnimationFrame'));

})(jQuery, window.Core);