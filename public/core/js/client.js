
/* client.js */

(function($) {
  
var wmin = 550;
var wmax = 992;

Core.once(':ready', function(body, html) {
  genHoverUI(body);
  genPaddingReduction(body);
  genFlowtype(body);
  initMinHeight(body);
  initPaddingReduction(body);
});

Core.once('ready', function(body, html) {
  initDropdownMenus(body);
  initSearchBox(body);
  initSections(body);
  initIconPosts(body);
  initHgallery(body);
  initHoverUI(body);
  initFlowtype(body);
});

Core.once('ready:', function() {
  Core.ui.renderStyles();
});

function genFlowtype(body) {
  body.find('.sv-progress-circle .inner-text').addClass('flowtype').attr({
    'data-flowtype-font-ratio': 14.5,
  });
}

function initFlowtype(body) {
  body.find('.flowtype').each(function(i, node) {
    var item = this.__self || (this.__self = $(this));
    var opts = Core.util.keysForPrefix('flowtype', item.data());
    opts = _.extend({
      minimum: 0,
      maximum: 992,
      fontRatio: 75
    }, opts);
    item.flowtype(opts);
  });
}

function genPaddingReduction(body) {
  
  // Only allow sections with the following bg-content elements
  var targets = ['span.fixed-bg', 'span.static-bg', 'img'].map(function(sel) {
    return '> ' + sel;
  });
  
  // Skip slider and sections that already have padding reduction set
  var items = body.find('section.block:not(.sv-slider):not([data-padding-reduction]) > .bg-content > .item');
  
  items.find(targets.join(', ')).parents('section.block').attr({
    'data-padding-reduction': 0.85
  });

}

function initPaddingReduction(body) {
  body.find('[data-padding-reduction]').each(function(i, node) {
    var item = $(node);
    var fac = item.data('padding-reduction');
    if (fac && fac < 1) {
      var fontSize = parseInt(item.css('fontSize'), 10);
      var padding = {
        top: parseFloat(item.css('paddingTop')).toFixed(3),
        bottom: parseFloat(item.css('paddingBottom')).toFixed(3)
      }
      var ems = {
        top: padding.top/fontSize,
        bottom: padding.bottom/fontSize
      }
      var style = Core.template.render('padding_reduction', {
        id: Core.ui.randId(item),
        md: {
          top: (fac*ems.top).toFixed(3) + 'em',
          bottom: (fac*ems.bottom).toFixed(3) + 'em',
        },
        sm: {
          top: (fac*fac*ems.top).toFixed(3) + 'em',
          bottom: (fac*fac*ems.bottom).toFixed(3) + 'em',
        },
        xs: {
          top: (fac*fac*fac*ems.top).toFixed(3) + 'em',
          bottom: (fac*fac*fac*ems.bottom).toFixed(3) + 'em'
        }
      });
      Core.ui.queueStyle(style);
    }
  });
}

function initMinHeight(body) {
  body.find('[data-min-height]').each(function(i, node) {
    var self = $(node);
    var opts = self.data();
    var getHeight = Core.util.interpolatePercent(opts.minHeight, self.height());
    Core.ui.resizeInterpolate({
      wmin: opts.wmin || wmin,
      wmax: opts.wmax || wmax
    }, function(v, w) {
      self.css({
        height: getHeight(v)
      });
    });
  });
}

function genHoverUI(body) {
  body.find('.sv-hover-data').each(function(i, node) {
    var self = $(node);
    var html = Core.template.render('sv_hover_ui', self.data('json'));
    self.replaceWith(html);
  });
}

function initHoverUI(body) {
  body.find('.sv-hover-ui').parent().addClass('sv-hover-parent');
}

function initHgallery(body) {
  body.find('.sv-hgallery').each(function(i, node) {
    var gallery = $(node);
    var ul = gallery.find('> .outer-wrap > .inner-wrap > ul.items');
    Core.ui.resizeTrigger({
      iterations: 4
    }, function(w) {
      ul.css({
        marginLeft: -1*Math.floor((ul.width() - w)/2),
        opacity: 1
      });
    });
  });
}

function initIconPosts(body) {
  body.find('.sv-icon-posts').each(function(i, node) {
    var container = $(node);
    var row = container.find('> .row');
    if (Core.ios) {
      var lastItem = null;
      row.delegate('> .item > a.icn', 'touchstart touchend', function(e) {
        var self = this.__self || (this.__self = $(this));
        var href = self.attr('href') || '';
        if (lastItem) lastItem.removeClass('hovering');
        lastItem = self.toggleClass('hovering');
        if (href === '' || href[0] === '#') {
          e.preventDefault();
        }
      });
    }
  });
}

function initDropdownMenus(body) {
  if (Core.mobile) return; // Mobile doesn't need a delay
  Core.vars.header.find('.nav-logo > ul.navigation > li').find('> ul.submenu, div.mega-menu').each(function(i, node) {
    var menu = $(node).addClass('noshow'); // Class must be added from js because IOS has problems with it
    var link = menu.siblings('a');
    var parent = link.parent();
    Core.ui.hoverDelay(link.add(menu), function() {
      parent.addClass('showmenu');
    }, function() {
      parent.removeClass('showmenu');
    }, 250);
  });
}

function initSections(body) {
  body.find('> section.block').each(function(i, node) {
    var section = this.__self || (this.__self = $(this));
    var fgContent = section.find('> .fg-content');
    var bgContent = section.find('> .bg-content');
    var fgItems = fgContent.find('> .item');
    var bgImages = bgContent.find('> .item > img');
    Core.ui.verticalCenter(bgImages, section);
    if (fgItems.hasClass('vcenter-show')) {
      Core.ui.verticalCenter(fgItems, section, bgImages);
    }
    Core.ui.resizeOnImagesLoaded(section);
  });
}

function initSearchBox(body) {
  var searchBox = Core.vars.searchBox;
  var parent = searchBox.parent();
  var events = new EventEmitter();
  var ease = {enter: 'easeOutBack', leave: 'easeInBack'};
  if (searchBox.length) {
    var visible = false, busy = false, t = 253;
    var mobileSearch = Core.vars.header.find('.nav-logo > .mobile-menu span.mobile-search-btn');
    var input = searchBox.find('input:text');
    var header = Core.vars.header;
    var searchIcon = header.find('ul.navigation > li.search > a');
    var escHandler = jwerty.event('esc', function() {
      searchIcon.trigger('click');
    });
    searchIcon.click(function(e) {
      if (busy) return; else busy = true;
      if (visible) {
        searchBox.transition({
          marginTop: -1*(searchBox.outerHeight()),
          opacity: 0
        }, 0.95*t, ease.leave, function() {
          body.unbind('keyup', escHandler);
          parent.hide();
          visible = busy = false;
        });
      } else {
        parent.show();
        searchBox.css({
          marginTop: -1*(searchBox.outerHeight()),
          visibility: 'visible',
          display: 'block'
        });
        searchBox.transition({
          marginTop: 0,
          opacity: 1
        }, t, ease.enter, function() {
          body.bind('keyup', escHandler);
          if (!Core.android) input.val(input.val()).trigger('focus'); // IOS doesn't receive focus this way
          visible = true; busy = false;
        });
      }
    });
    searchBox.find('span.cancel-search').add(mobileSearch).click(function(e) {
      searchIcon.trigger('click');
      e.preventDefault();
    });
  }
}

})(jQuery);