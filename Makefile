
default:
		@echo; echo "Actions: clean | lint | lint-client | lint-server | fixperms"; echo

clean:
		@rm -f public/core/*.css
		@find public/core/css -type f -iname *.css -exec rm -f {} \;

lint-client:
		@protos lint public/core/js/*.js --config jshint-client.json
		
lint-server:
		@protos lint api app config env ext hook include boot.js --config jshint-server.json
		
lint: lint-server lint-client
		
fixperms:
		@find . -type f \! \( -path "*/.git/*" -or -path "*/node_modules/*" \) -exec chmod 664 {} \;
		@find . -type d \! \( -path "*/.git/*" -or -path "*/node_modules/*" \) -exec chmod 775 {} \;
		
-include Makefile.git.mk