
git-local:
		@unlink .git
		@ln -s .git-local .git
		
git-remote:
		@unlink .git
		@ln -s .git-remote .git

git-temp-init:
		@./scripts/git-temp.sh
