#!/bin/bash

if [ -f ".git/IS_LOCAL_REPO" ]; then
  git add --all
  git commit -m update
else
  echo "This script should not be run on remote repository"
fi
